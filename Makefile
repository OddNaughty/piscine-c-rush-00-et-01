# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: plantran <plantran@42.fr>                    +#+  +:+       +#+       #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/24 15:07:43 by plantran          #+#    #+#              #
#    Updated: 2015/02/17 04:20:43 by plantran         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
################################################################################
###############													 ###############
###############					   MAKEFILE						 ###############
###############													 ###############
################################################################################

################
## VARIABLES  ##
################

#SOURCES#
#SRC_NAME	=	main.c		\

SRC_PATH	=	./srcs/
SRC			=	$(addprefix $(SRC_PATH), $(SRC_NAME))

#OBJETS#

OBJ_NAME	=	$(SRC_NAME:.c=.o)
OBJ_PATH	=	./obj/
OBJ			=	$(addprefix $(OBJ_PATH), $(OBJ_NAME))

#INCLUDES & LIBRAIRIES#

INC_LIBFT_PATH	=	./libft/includes/
LIB_PATH		=	./libft/libft.a
LIBFT_PATH		=	./libft/
INC_PATH		=	./includes/
NAME			=	game_2048

#COMPILATEUR#

CC			=	gcc
FLAGS		=	-Wall -Werror -Wextra


#################
##   CIBLES    ##
#################

all: $(NAME)

$(OBJ_PATH):
	@if [ ! -d "$(OBJ_PATH)" ]; then \
	mkdir $(OBJ_PATH);\
	fi

$(OBJ): | $(OBJ_PATH)

$(NAME): $(OBJ)
	$(CC) $(FLAGS) main.c -I $(INC_LIBFT_PATH) -I $(INC_PATH) -o $@ -c  $<

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	$(CC) $(FLAGS) -I $(INC_LIBFT_PATH) -I $(INC_PATH) -o $@ -c  $<

clean:
	@rm -rf $(OBJ_PATH)
	@make clean -C ./libft

fclean: clean
	@rm -fv $(NAME)
	@make fclean -C ./libft

re : fclean all
