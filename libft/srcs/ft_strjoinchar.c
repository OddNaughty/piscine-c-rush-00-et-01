/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoinchar.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <plantran@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/17 05:22:03 by plantran          #+#    #+#             */
/*   Updated: 2015/02/17 05:27:06 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strjoinchar(char *s1, char c)
{
	size_t			len;
	char			*new_s;
	int				i;

	i = 0;
	if (s1 == NULL)
		len = 1;
	if (s1)
		len = ft_strlen(s1 + 1);
	new_s = ft_strnew(len + 1);
	new_s = ft_strjoin(new_s, s1);
	while (s1 && s1[i])
		i++;
	new_s[i] = c;
	new_s[i + 1] = '\0';
	return (new_s);
}
