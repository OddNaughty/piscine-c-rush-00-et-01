/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 19:26:30 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 14:12:33 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

static int	ft_strwasfound(const char *s1, const char *s2, size_t i, size_t n)
{
	size_t	j;

	j = 0;
	while (s1[i] && s2[j] && s1[i] == s2[j] && i < n)
	{
		i++;
		j++;
	}
	if (s2[j] == '\0')
		return (1);
	return (0);
}

char		*ft_strnstr(char const *s1, char const *s2, size_t n)
{
	size_t			i;
	int				j;

	i = 0;
	j = 0;
	while (s1[i] && i <= n + 1)
	{
		if ((s2[j] == s1[i]) && (ft_strwasfound(s1, s2, i, n) == 1))
			return ((char*)s1 + i);
		i++;
	}
	return (NULL);
}
