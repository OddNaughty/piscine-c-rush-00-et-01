/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 16:07:04 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 13:54:17 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <string.h>

void	*ft_memalloc(size_t size)
{
	void			*tab;

	if (!size)
		return (NULL);
	tab = (void*)malloc(size * sizeof(tab));
	if (tab == NULL)
		return (NULL);
	ft_bzero(tab, size);
	return (tab);
}
