/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 21:39:31 by plantran          #+#    #+#             */
/*   Updated: 2014/12/18 08:07:15 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <string.h>

static	char		*ft_strcutcar(char const *s, int c)
{
	size_t	i;
	size_t	j;
	char	*s1;

	i = 0;
	s1 = NULL;
	if (s)
	{
		j = ft_strlen(s);
		while (s[i] == c)
			i++;
		while ((s[j - 1] == c && j > i))
			j--;
		s1 = ft_strsub(s, i, j - i);
	}
	return (s1);
}

static	int			ft_nbstr(char const *s, int c)
{
	int						nb;
	int						i;

	nb = 0;
	i = 0;
	while (s[i])
	{
		if (s[i] && s[i] != c)
		{
			while (s[i] && s[i] != c)
				i++;
			nb++;
		}
		if (s[i] != '\0')
			i++;
	}
	return (nb);
}

static	int			ft_isend(char const *s, int c, int i)
{
	if (s[i] && s[i] != c)
	{
		while (s[i] != c && s[i] != '\0')
			i++;
	}
	return (i);
}

static	char		**ft_resolve(char **tab, const char *s, int c, int i)
{
	int		k;
	int		start;

	k = 0;
	start = i;
	while (s[i] != '\0')
	{
		if (s[i] != c)
		{
			i = ft_isend(s, c, i);
			tab[k] = ft_strsub(s, start, (i - start));
		}
		if (s[i] == c)
		{
			while (s[i] == c && s[i])
			{
				i++;
				start = i;
			}
		}
		k++;
	}
	tab[k] = '\0';
	return (tab);
}

char				**ft_strsplit(char const *s, int c)
{
	char	**tab;
	int		nb;
	int		i;

	if (!s)
		return (NULL);
	i = 0;
	nb = ft_nbstr(s, c);
	s = ft_strcutcar(s, c);
	if (!(tab = (char**)malloc(sizeof(char*) * (nb + 1))))
		return (NULL);
	ft_resolve(tab, s, c, i);
	return (tab);
}
