/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 11:08:12 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 14:10:33 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t				i;
	unsigned const char	*t_s1;
	unsigned const char	*t_s2;

	t_s1 = (unsigned const char*)s1;
	t_s2 = (unsigned const char*)s2;
	if (n < 1)
		i = -1;
	else
		i = 0;
	while (t_s1[i] && t_s2[i] && t_s1[i] == t_s2[i] && i < (n - 1))
		i++;
	return (t_s1[i] - t_s2[i]);
}
