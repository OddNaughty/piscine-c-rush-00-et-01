/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/08 15:48:08 by plantran          #+#    #+#             */
/*   Updated: 2015/01/03 02:21:49 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	**ft_tabsub(char **tab, int start)
{
	int				k;
	int				i;
	char			**t_tab;

	k = 0;
	i = 0;
	t_tab = (char**)malloc(4096 * sizeof(char**) + 1);
	while (tab[i])
		i++;
	while (start < i)
	{
		t_tab[k] = ft_strdup(tab[start]);
		k++;
		start++;
	}
	t_tab[k] = '\0';
	return (t_tab);
}
