/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/08 12:32:56 by plantran          #+#    #+#             */
/*   Updated: 2014/11/19 16:12:01 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <string.h>

char	*ft_strdup(const char *s1)
{
	char			*new_s;

	new_s = ft_strnew(ft_strlen(s1));
	if (new_s == NULL)
		return (NULL);
	ft_strcpy(new_s, s1);
	return (new_s);
}
