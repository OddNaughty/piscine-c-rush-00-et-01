/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdelone.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/12 12:46:30 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 14:18:27 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>
#include <stdlib.h>

void	ft_lstdelone(t_list **alst, void (*del)(void*, size_t))
{
	t_list			*list;

	if (alst != NULL && *alst != NULL && del != NULL)
	{
		list = *alst;
		del(list->content, list->content_size);
		free (list);
		*alst = NULL;
	}
}
