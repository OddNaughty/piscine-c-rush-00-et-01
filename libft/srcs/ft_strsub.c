/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 16:09:24 by plantran          #+#    #+#             */
/*   Updated: 2014/11/18 16:42:51 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	size_t		i;
	char		*new_s;

	i = 0;
	if (!s)
		return (NULL);
	new_s = ft_strnew(len);
	if (new_s == NULL)
		return (NULL);
	while (s[start] && i < len)
	{
		new_s[i] = s[start];
		i++;
		start++;
	}
	return (new_s);
}
