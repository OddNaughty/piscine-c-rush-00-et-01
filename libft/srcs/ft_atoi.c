/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 11:56:17 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 13:48:35 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_atoi(const char *nptr)
{
	int		i;
	int		nb;
	int		sign;

	i = 0;
	nb = 0;
	sign = 1;
	while (nptr[i] == ' ' || nptr[i] == '\n' || nptr[i] == '\t'
			|| nptr[i] == '\r' || nptr[i] == '\v' || nptr[i] == '\f')
		i++;
	if ((nptr[i] == '-' || nptr[i] == '+') && ft_isdigit(nptr[i + 1]) != 0)
	{
		sign = ((nptr[i] == '-') ? -1 : 1);
		i++;
	}
	while (ft_isdigit(nptr[i]) != 0)
	{
		nb = ((nb * 10) + (nptr[i] - '0'));
		i++;
	}
	return (nb * sign);
}
