/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 10:35:29 by plantran          #+#    #+#             */
/*   Updated: 2015/02/17 05:39:53 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strjoin(char const *s1, char const *s2)
{
	size_t			len;
	char			*new_s;

	if (!s1 && s2)
		return (ft_strdup(s2));
	if (s1 && !s2)
		return (ft_strdup(s1));
	if (!s1 && !s2)
		return (NULL);
	len = (ft_strlen(s1) + ft_strlen(s2));
	if (!(new_s = ft_strnew(len)))
		return (NULL);
	ft_strcpy(new_s, s1);
	ft_strcat(new_s, s2);
	return (new_s);
}
