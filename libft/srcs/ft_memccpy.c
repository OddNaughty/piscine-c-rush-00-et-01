/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 19:19:34 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 13:54:45 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	char			*t_src;
	char			*t_dst;
	unsigned char	k;
	int				i;

	t_src = (char*)src;
	t_dst = (char*)dst;
	k = (unsigned char)c;
	i = 0;
	if (t_src == NULL || t_dst == NULL)
		return (NULL);
	while (i < (int)n)
	{
		t_dst[i] = t_src[i];
		if ((unsigned char)t_dst[i] == k)
			return (t_dst + i + 1);
		i++;
	}
	return (NULL);
}
