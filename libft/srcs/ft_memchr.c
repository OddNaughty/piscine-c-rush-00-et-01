/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 20:35:16 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 13:55:05 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void	*ft_memchr(const void *s, int c, size_t n)
{
	size_t						i;
	unsigned char				temp_c;
	unsigned const char			*t_s;

	i = 0;
	temp_c = (unsigned char)c;
	t_s = (unsigned const char*)s;
	while (t_s[i] != temp_c && i < (n - 1))
		i++;
	if (t_s[i] == temp_c)
		return ((char*)t_s + i);
	else
		return (NULL);
}
