/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 11:02:23 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 14:03:13 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strcmp(const char *s1, const char *s2)
{
	int					i;
	unsigned const char *t_s1;
	unsigned const char *t_s2;

	i = 0;
	t_s1 = (unsigned const char*)s1;
	t_s2 = (unsigned const char*)s2;
	while (t_s1[i] && t_s2[i] && t_s1[i] == t_s2[i])
		i++;
	return (t_s1[i] - t_s2[i]);
}
