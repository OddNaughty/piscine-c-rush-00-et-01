/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/06 11:18:46 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 13:56:49 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	char			*tab_dst;
	char			*tab_src;

	tab_dst = (char*)dst;
	tab_src = (char*)src;
	if (tab_src <= tab_dst)
	{
		while (len > 0)
		{
			tab_dst[len - 1] = tab_src[len - 1];
			len--;
		}
	}
	else
		ft_memcpy(tab_dst, tab_src, len);
	return (tab_dst);
}
