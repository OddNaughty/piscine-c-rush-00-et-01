/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoindel.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/03 02:29:18 by plantran          #+#    #+#             */
/*   Updated: 2015/01/03 02:29:21 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "stdlib.h"

char	*ft_strjoindel(char *s1, char *s2)
{
	size_t		len;
	char		*temp;

	if (!s1 && s2)
	{
		temp = ft_strdup(s2);
		return (temp);
	}
	if (s1 && !s2)
	{
		temp = ft_strdup(s1);
		free(s1);
		return (temp);
	}
	len = (ft_strlen(s1) + ft_strlen(s2));
	if (!(temp = ft_strnew(len)))
		return (NULL);
	ft_strcpy(temp, s1);
	ft_strcat(temp, s2);
	free(s1);
	return (temp);
}
