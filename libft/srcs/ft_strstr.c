/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/04 18:17:56 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 14:15:09 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

char	*ft_strstr(const char *s1, const char *s2)
{
	int			i;
	int			j;
	int			len;

	i = 0;
	j = 0;
	len = ft_strlen(s2);
	while (s1[i])
	{
		while (s2[j] && s1[i] == s2[j])
		{
			i++;
			j++;
		}
		if (j == len)
			return ((char*)s1 + i - j);
		i++;
		j = 0;
	}
	return (NULL);
}
