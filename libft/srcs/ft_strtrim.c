/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 21:41:29 by plantran          #+#    #+#             */
/*   Updated: 2014/11/23 16:34:54 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>
#include "libft.h"

static int	ft_wheretalpha(char *s)
{
	int						i;

	i = 0;
	while (s[i] && (s[i] == ' ' || s[i] == ',' || s[i] == '\n' || s[i] == '\t'))
		i++;
	return (i);
}

static char	*ft_strrev(char *s)
{
	size_t				i;
	size_t				j;
	char				*temp_str;

	i = 0;
	j = 0;
	if (s == NULL)
		return (NULL);
	temp_str = (char*)malloc(ft_strlen(s));
	if (temp_str == NULL)
		return (NULL);
	while (s[i])
		i++;
	while (j < ft_strlen(s))
	{
		temp_str[j] = s[i - 1];
		j++;
		i--;
	}
	return (temp_str);
}

char		*ft_strtrim(char const *s)
{
	size_t				i;
	size_t				j;
	int					k;
	char				*new_s;

	if (s == NULL)
		return (NULL);
	i = ft_wheretalpha((char*)s);
	j = ft_wheretalpha(ft_strrev((char*)s));
	k = 0;
	if (i == ft_strlen(s))
		new_s = (char*)ft_memalloc(1);
	else
	{
		new_s = ft_strnew(ft_strlen(s) - (i + j));
		if (!new_s)
			return (NULL);
		while (i < (ft_strlen(s) - j))
		{
			new_s[k] = s[i];
			k++;
			i++;
		}
	}
	return (new_s);
}
