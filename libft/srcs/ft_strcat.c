/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcat.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/03 11:28:44 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 14:02:25 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcat(char *s1, const char *s2)
{
	int			i;
	int			j;
	char		*temp_dest;

	i = 0;
	j = 0;
	temp_dest = s1;
	while (s1[i])
		i++;
	while (s2[j])
	{
		temp_dest[i] = s2[j];
		i++;
		j++;
	}
	temp_dest[i] = '\0';
	return (temp_dest);
}
