/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/13 11:27:52 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 14:09:11 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t			len;

	len = ft_strlen(dst);
	if (len >= size)
	{
		return (ft_strlen(src) + size);
	}
	else
	{
		ft_strncat(dst, src, (size - len - 1));
		return (ft_strlen(src) + len);
	}
}
