/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 10:28:18 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 14:11:37 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t					i;
	unsigned const char		*t_s1;
	unsigned const char		*t_s2;

	if (!s1 && !s2)
		return (0);
	if (n == 0)
		i = -1;
	else
		i = 0;
	t_s1 = (unsigned const char*)s1;
	t_s2 = (unsigned const char*)s2;
	while (t_s1[i] && t_s2[i] && t_s1[i] == t_s2[i] && i < (n - 1))
		i++;
	if (t_s1[i] - t_s2[i] == 0)
		return (1);
	else
		return (0);
}
