/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 20:57:31 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 13:55:25 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

int	ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t				i;
	unsigned const char	*t_s1;
	unsigned const char	*t_s2;

	if (n < 1)
		i = -1;
	else
		i = 0;
	t_s1 = (unsigned const char*)s1;
	t_s2 = (unsigned const char*)s2;
	while (t_s1[i] == t_s2[i] && i < (n - 1))
		i++;
	return (t_s1[i] - t_s2[i]);
}
