/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/05 18:12:52 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 13:56:00 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memcpy(void *dst, const void *src, size_t n)
{
	size_t		i;
	char		*tab_src;
	char		*tab_dst;

	i = 0;
	tab_src = (char*)src;
	tab_dst = (char*)dst;
	while (i < n)
	{
		tab_dst[i] = tab_src[i];
		i++;
	}
	return (tab_dst);
}
