/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/12 13:09:38 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 13:52:58 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>
#include <string.h>

void	ft_lstdel(t_list **alst, void (*del)(void*, size_t))
{
	t_list		*list;

	if (alst != NULL && *alst != NULL && del != NULL)
	{
		list = *alst;
		while (list->next != NULL)
		{
			del(list->content, list->content_size);
			free (list);
			list = list->next;
		}
		*alst = NULL;
	}
}
