/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: plantran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/07 11:10:07 by plantran          #+#    #+#             */
/*   Updated: 2014/11/13 13:50:23 by plantran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

static char	*ft_charnbr(int n, int temp_n, int i, char *tab)
{
	int					k;

	k = 0;
	if (temp_n == 1)
	{
		tab[k] = '-';
		k = 1;
	}
	while (i > 0)
	{
		tab[k] = ((n / i) % 10 + '0');
		k++;
		i /= 10;
	}
	return (tab);
}

char		*ft_itoa(int n)
{
	int			i;
	int			len;
	int			temp_n;
	char		*tab;

	i = 1;
	len = 1;
	temp_n = (n < 0 ? 1 : 0);
	if (n == -2147483648)
	{
		if (!(tab = ft_strnew(11)))
			return (NULL);
		return (tab = "-2147483648");
	}
	if (n < 0)
		n = -n;
	while ((n / i) > 9)
	{
		i *= 10;
		len++;
	}
	if (!(tab = ft_strnew(len + temp_n)))
		return (NULL);
	tab = ft_charnbr(n, temp_n, i, tab);
	return (tab);
}
