#ifndef GAMEBOARD_CLASS_HPP
# define GAMEBOARD_CLASS_HPP

#include <ncurses.h>
#include <iostream>
#include "AEntity.class.hpp"
#include "Player.class.hpp"

class GameBoard {
public:
	GameBoard(void);
	GameBoard(const int life, const t_coo size);
	GameBoard(GameBoard const & src);
	~GameBoard(void);

	GameBoard &operator=(GameBoard const &);

	AEntity			****getMap(void) const;
	t_coo			getCoo(void) const;

	void            addToCase(t_coo coo, AEntity *entity);
	void            deleteFromCase(t_coo coo, AEntity *ref);
	void			update(int key);
	void			display(void);
	int				turn;
	t_coo			getSize(void);
	void			resetHasPlayed(void);
	void			gameOver();
	void            addScore(int points);

private:
	unsigned int    _score;
	Player		*_player;
	AEntity		****_map;
	t_coo		_sizeMap;
	bool		_checkEntry(void);
	bool		_caseIsFull(int y, int x);
	void		_addEnemy(void);
	void		_remap(AEntity & entity);
	int			_frames;

};
//std::ostream & operator <<(std::ostream & o, const AEntity  & src);
#endif