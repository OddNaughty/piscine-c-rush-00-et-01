#include "GameBoard.class.hpp"
#include "GeneralHeader.hpp"
#include "Basic.hpp"
#include "Rusher.hpp"
#include <iostream>
#include <cstdlib>

//##############################################################################
// Constructors / Destructors ##################################################

GameBoard::GameBoard(const int life, const t_coo size)
        :  turn(0) , _score(0), _sizeMap(size), _frames(-1) {
    t_coo playerCoords = {(size.x - 1) / 2, size.y - 1};
    this->_player = new Player(this, playerCoords, life);
    _map = new AEntity***[size.y];
    for (unsigned int i = 0; i < size.y; ++i) {
        _map[i] = new AEntity**[size.x];
        for (unsigned int j = 0; j < size.x; ++j) {
            _map[i][j] = new AEntity*[ENEMY_PER_CASE];
        }
    }
    for (unsigned int i = 0; i < size.y; ++i) {
        for (unsigned int j = 0; j < size.x; ++j) {
            for (int k = 0; k < ENEMY_PER_CASE; ++k) {
                _map[i][j][k] = NULL;
            }
        }
    }
    this->_map[playerCoords.y][playerCoords.x][0] = this->_player;
}

GameBoard::~GameBoard(void) {
    for (unsigned int i = 0; i < _sizeMap.y; ++i) {
        for (unsigned int j = 0; j < _sizeMap.x; ++j) {
            if (_map[i][j] != NULL)
                delete _map[i][j];
        }
        delete _map[i];
    }
    delete _map;
}

//##############################################################################
// Operators ###################################################################

//##############################################################################
// Getters/Setters #############################################################

AEntity			****GameBoard::getMap(void) const{
    return this->_map;
}

t_coo			GameBoard::getCoo(void) const{
    return this->_sizeMap;
}

void            GameBoard::deleteFromCase(t_coo coo, AEntity *ref) {
    (void) ref;
    this->_map[coo.y][coo.x][ref->getNumber()] = NULL;
    (void)  coo;
}

void            GameBoard::addToCase(t_coo coo, AEntity *entity){
    int i = 0;
    while (i < ENEMY_PER_CASE && _map[coo.y][coo.x][i] != NULL) {i++;}
    if (i <= ENEMY_PER_CASE) {
        this->_map[coo.y][coo.x][i] = entity;
        entity->setNumber(i);
    }
}

t_coo           GameBoard::getSize() {
    return this->_sizeMap;
}
//##############################################################################
// Publics functions ###########################################################

void			GameBoard::update(int key) {
    (void) key;
    if (_frames == FPF)
        _frames = 0;
    else
        _frames++;
    if (!(_frames % FPF)) {
        _addEnemy();
    }
    for (unsigned int i = 0; i < _sizeMap.y; ++i) {
        for (unsigned int j = 0; j < _sizeMap.x; ++j) {
            for (int k = 0; k < ENEMY_PER_CASE; ++k) {
                if (_map[i][j][k] != NULL) {
                    _map[i][j][k]->update(key);
                }
            }
        }
    }
    turn++;
}

void            GameBoard::_remap(AEntity &entity) {
    (void)entity;
//    t_coo   pos = entity.
}

bool            GameBoard::_checkEntry(void){
    for (unsigned int i = 0; i < _sizeMap.x; ++i) {
        for (int j = 0; j < ENEMY_PER_CASE; ++j) {
            if (!_map[0][i][j]) {
                return (true);
            }
        }
    }
    return false;
}

bool            GameBoard::_caseIsFull(int y, int x) {
    int i = 0;
    while (i < ENEMY_PER_CASE && _map[y][x][i] != NULL){i++;}
    return (i == ENEMY_PER_CASE);
}

void            GameBoard::_addEnemy(void) {
    if (_checkEntry()) {
        if (turn % 2) {
            unsigned int enemyPosition = rand() % (_sizeMap.x);
            if (!_caseIsFull(0, enemyPosition)) {
                t_coo coos = {enemyPosition, 0};
                Basic *dirty = new Basic(this, coos);
                addToCase(coos, dirty);
//                coos.x = (coos.x + 1) % _sizeMap.x;
//                Rusher *dirty2 = new Rusher(this, coos);
//                addToCase(coos, dirty2);
            }
        }
        else if (turn % 5) {
            unsigned int enemyPosition = rand() % (_sizeMap.x);
            if (!_caseIsFull(0, enemyPosition)) {
                t_coo coos = {enemyPosition, 0};
                Rusher *dirty = new Rusher(this, coos);
                addToCase(coos, dirty);
            }
        }
    }
}

void			GameBoard::display(void) {
    t_coo   size = this->getCoo();
    for (unsigned int i = 0; i < size.y; ++i) {
        for (unsigned int j = 0; j < size.x; ++j) {
            if (this->_map[i][j][0] == NULL)
                mvaddch(i, j, ' ');
            else
                mvaddch(i, j, this->_map[i][j][0]->getSymbol());
        }
    }
    refresh();
}

void            GameBoard::addScore(int points){
    this->_score += points;
}

void			GameBoard::resetHasPlayed(void){
    for (unsigned int i = 0; i < _sizeMap.y; ++i) {
        for (unsigned int j = 0; j < _sizeMap.x; ++j) {
            for (int k = 0; k < ENEMY_PER_CASE; ++k) {
                if (this->_map[i][j][k] != NULL)
                    this->_map[i][j][k]->setPlayed(false);
            }
        }
    }
    mvprintw(_sizeMap.y + 1, 1,"LIFE:   %d\tSCORE:   %d", this->_player->getLife(), this->_score);
}

void            GameBoard::gameOver() {
    init_color(COLOR_RED, 700, 0, 0);
    nodelay(stdscr, FALSE);

    mvprintw(2, 2,"GAME OVER");
    mvprintw(3, 2,"PRESS ANY KEY TO QUIT");
    refresh();
    sleep(1);
    int ch = getch();
    (void)ch;
    endwin();
    exit(0);
}

//##############################################################################
// Private functions ###########################################################
