#include "AEnemy.class.hpp"
#include <iostream>
#include "GameBoard.class.hpp"
//##############################################################################
// Constructors / Destructors ##################################################

AEnemy::AEnemy(GameBoard *board, const char c, const t_coo start, unsigned int score, int life, unsigned int damages, int velocity)
		: AEntity::AEntity(board, c, start, life, damages, ENEMY, velocity), _scoreWorth(score) {
//	DIS(BLUE_COLOR, "Je suis un nouvel enemy et mon symbole est: " << _symbol << " et ma position est " << _pos.y << "/" << _pos.x);
}

AEnemy::~AEnemy() {}

//##############################################################################
// Operators ###################################################################


//##############################################################################
// Getters/Setters #############################################################

//##############################################################################
// Publics functions ###########################################################

void		AEnemy::_move() {}

void		AEnemy::update(int key) {
	if (_frames == FPF)
		_frames = 0;
	else
		_frames++;
	(void) key;
	if (_dead)
		return _die();
	if (!(_frames % _velocity))
		_move();
	if (_dead)
		return _die();
}
//##############################################################################
// Private functions ###########################################################

void		AEnemy::getHit(int damages) {
	AEntity::getHit(damages);
	if (_dead)
		this->_board->addScore(_scoreWorth);
}

void		AEnemy::_manageHits(t_coo next) {
	for (int i = 0; i < ENEMY_PER_CASE; ++i) {
		if (this->_board->getMap()[next.y][next.x][i] != NULL && this->_board->getMap()[next.y][next.x][i]->getType() != ENEMY) {
			this->_board->getMap()[next.y][next.x][i]->getHit(_damages);
			this->getHit(this->_board->getMap()[next.y][next.x][i]->getDamages());
			if (_dead) {
				return;
			}
		}
	}
}
