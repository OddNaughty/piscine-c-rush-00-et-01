#include "Basic.hpp"
#include "GameBoard.class.hpp"
#include "Projectile.hpp"
#include "Bullet.hpp"

Basic::Basic(GameBoard *board, const t_coo start)
        : AEnemy(board, 'X', start, 10, 1, 1, 10) {}

Basic::~Basic(void) {

}

void    Basic::update(int key) {
    AEnemy::update(key);
    if (this->_board->turn % (FPF % 120))
        shoot();
}

void    Basic::shoot() {
    bool	shot = false;
    t_coo   size = this->_board->getCoo();
    for (int i = 0; i < ENEMY_PER_CASE; ++i) {
        if ((_pos.y + 1) < size.y && this->_board->getMap()[_pos.y + 1][_pos.x][i] != NULL && this->_board->getMap()[_pos.y + 1][_pos.x][i]->getType() != ENEMY) {
            this->_board->getMap()[_pos.y + 1][_pos.x][i]->getHit(_damages);
            shot = true;
            break;
        }
    }
    if (shot || _pos.y + 1 >= size.y)
        return;
    t_coo proj = {_pos.x, _pos.y + 1};
    Projectile *bullet = new Bullet(this->_board, proj, false);
    this->_board->addToCase(proj, bullet);
}


void    Basic::_move(void) {
    if (!this->_hasPlayed) {
        t_coo sizeMap = this->_board->getSize();
        t_coo newCoords;
        t_coo oldCoords;
        newCoords = oldCoords = this->getPos();

        int newPosy = newCoords.y <  sizeMap.y - 1? newCoords.y + 1 : newCoords.y;

        newCoords.y = newPosy;
        if (newPosy == (int)sizeMap.y - 1) {
            this->_board->deleteFromCase(oldCoords, this);
            if (newCoords.y == oldCoords.y){
                return _die();
            }
        }
        _manageHits(newCoords);
        if (_dead)
            return ;
        _pos = newCoords;
        this->setPlayed(true);
        this->_board->deleteFromCase(oldCoords, this);
        this->_board->addToCase(newCoords, this);
    }
}