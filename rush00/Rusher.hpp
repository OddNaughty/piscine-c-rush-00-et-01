#ifndef __RUSHER_H_
 #define __RUSHER_H_

#include "AEnemy.class.hpp"

class Rusher : public AEnemy {
public:
    Rusher(void);
    Rusher(GameBoard *board, const t_coo start);
    Rusher(Rusher const & src);
    ~Rusher(void);

    Rusher & operator=(Rusher const & rhs);

    void        update(int key);
private:
    void        _move(void);};


#endif
