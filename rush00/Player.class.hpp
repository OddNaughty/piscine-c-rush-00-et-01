#ifndef PLAYER_CLASS_HPP
 #define PLAYER_CLASS_HPP

#include "AEntity.class.hpp"

class Player : public AEntity{
public:
	Player(void);
	Player(GameBoard *board, t_coo coords, int life);
	Player(const Player & src);
	~Player(void);
	Player &operator=(Player const &rhs);

	void		update(int key);
	void		move(t_coo coords);
	void		getHit(int damages);
	int			getLife(void);

private:
	void		_die(void);
	void		_move(int key);
	void		_move(void);
	void		_shoot(void);
	void		_manageHits(t_coo next);
};

#endif