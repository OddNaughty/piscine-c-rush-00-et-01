#ifndef __GENERAL_HEADER_H_
 #define __GENERAL_HEADER_H_

#define BLUE_COLOR "\033[1;36m"
#define YELLOW_COLOR "\e[0;33m"
#define DEFAULT "\e[0m"
#define DIS(color, say) std::cout << color << say << DEFAULT << std::endl;
//#define DEBUG(x)

#define ENEMY_PER_CASE 5
#define FPF 60

#define PLAYER 0
#define ENEMY 1
#define PROJECTILE 2


#include <sys/ioctl.h>
#include <stdio.h>
#include <unistd.h>
typedef struct  s_coo {
    unsigned int         x;
    unsigned int         y;
}               t_coo;

#endif
