#include "Bullet.hpp"

Bullet::Bullet(GameBoard *board, const t_coo start, bool fromPlayer)
        : Projectile(board, '|', start, fromPlayer, 1, 2) {
}

Bullet::~Bullet(void) {}

void        Bullet::update(int key) {
    Projectile::update(key);
}

void        Bullet::_move() {
    Projectile::_move();
}