#include "AEntity.class.hpp"
#include "GameBoard.class.hpp"
#include <iostream>

AEntity::AEntity(void){
}

AEntity::AEntity(GameBoard * board, const char symbol, const t_coo start, int life, int damages, int type, int velocity)
		:  _symbol(symbol), _pos(start), _board(board), _dead(false), _life(life), _damages(damages), _type(type), _velocity(velocity), _frames(0) {}

AEntity::AEntity(const AEntity & src){
	(void)src;
}

AEntity::~AEntity(void){
}

char AEntity::getSymbol() {
	return _symbol;
}

t_coo	AEntity::getPos() {
	return _pos;
}

bool				AEntity::getHasPlayed(void) const{
	return this->_hasPlayed;
}

void				AEntity::setPlayed(bool played){
	this->_hasPlayed = played;
}

void				AEntity::_die(void) {
	delete _board->getMap()[_pos.y][_pos.x][_numberOnCase];
	_board->deleteFromCase(_pos, this);
}

void				AEntity::setNumber(int i) {
	_numberOnCase = i;
}

void				AEntity::getHit(int damages) {
	_life -= damages;
	if (_life < 0)
		_dead = true;
}

int					AEntity::getNumber() {
	return _numberOnCase;
}

int					AEntity::getDamages() {
	return _damages;
}

int					AEntity::getType() {
	return _type;
}
void				AEntity::kill() {
	_dead = true;
}

void				AEntity::_manageHits(t_coo next) {(void) next;}
