#include "Player.class.hpp"
#include <stdlib.h>
#include "GameBoard.class.hpp"
#include "Projectile.hpp"
#include "Bullet.hpp"

Player::Player(GameBoard *board, t_coo coords, int life) : AEntity(board, 'O', coords, life, 100000, PLAYER, 2) {
}

Player::~Player(void){
}

void		Player::update(int key) {
	if (_frames == FPF)
		_frames = 0;
	else
		_frames++;
	if (_dead)
		return _die();
	if (_frames % (FPF/_velocity)) {
		if (key == ' ')
			_shoot();
		_move(key);
	}
	if (_dead)
		return _die();
}
int			Player::getLife(void) {
	return this->_life;
}
void		Player::_move(int key) {
	if (!this->_hasPlayed) {
		t_coo sizeMap = this->_board->getSize();
		t_coo newCoords;
		t_coo oldCoords;
		newCoords = oldCoords = this->getPos();


		if (key == KEY_UP) {
			endwin();
			exit(0);
		}
		if (key == KEY_LEFT && newCoords.x > 0) {
			newCoords.x -= 1;
			this->_pos = newCoords;
		}
		else if (key == KEY_RIGHT && newCoords.x < sizeMap.x - 1) {
			newCoords.x += 1;
			this->_pos = newCoords;
		}
		this->setPlayed(true);
		this->_board->deleteFromCase(oldCoords, this);
		this->_board->addToCase(newCoords, this);
	}
}

void		Player::_move(void){
}

void		Player::_shoot(void) {
	bool	shot = false;
	for (int i = 0; i < ENEMY_PER_CASE; ++i) {
		if (this->_board->getMap()[_pos.y - 1][_pos.x][i] != NULL && this->_board->getMap()[_pos.y - 1][_pos.x][i]->getType() == ENEMY) {
			this->_board->getMap()[_pos.y - 1][_pos.x][i]->getHit(_damages);
			shot = true;
			break;
		}
	}
	if (shot)
		return;
	t_coo proj = {_pos.x, _pos.y - 1};
	Projectile *bullet = new Bullet(this->_board, proj, true);
	this->_board->addToCase(proj, bullet);
}

void		Player::getHit(int damages) {
	(void) damages;
	usleep(500);
	_life--;
	if (_life < 0){
		_dead = true;
		_life = 0;
	}
	t_coo   size = this->_board->getCoo();
	mvprintw(size.y + 1, 1,"LIFE:   %d", this->getLife());
}

void		Player::_manageHits(t_coo next) {
	bool		hitted = false;
	for (int i = 0; i < ENEMY_PER_CASE; ++i) {
		if (this->_board->getMap()[next.y][next.x][i] != NULL) {
			this->_board->getMap()[next.y][next.x][i]->kill();
			hitted = true;
		}
	}
	if (hitted)
		this->getHit(15);
}

void		Player::_die() {
	this->_board->gameOver();
	AEntity::_die();
}