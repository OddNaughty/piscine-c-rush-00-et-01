#ifndef __BULLET_H_
 #define __BULLET_H_

#include "Projectile.hpp"

class Bullet : public Projectile {
public:
    Bullet(void);
    Bullet(GameBoard *board, const t_coo start, bool fromPlayer);
    Bullet(Bullet const & src);
    ~Bullet(void);
    
    Bullet & operator=(Bullet const & rhs);

    void            update(int key);
private:
    void            _move(void);
};


#endif
