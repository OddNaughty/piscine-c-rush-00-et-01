#ifndef AENTITY_CLASS_HPP
 # define AENTITY_CLASS_HPP

#include "GeneralHeader.hpp"
#include <ncurses.h>
#include <iostream>
#include <unistd.h>

class GameBoard;

class AEntity{
public:
	AEntity(void);
	AEntity(GameBoard *board, const char symbol, const t_coo start, int life, int damages, int type, int velocity);
	AEntity(const AEntity & src);
	virtual ~AEntity(void);
	AEntity &operator=(AEntity const &rhs);

	char				getSymbol(void);
	t_coo				getPos(void);

	virtual void        update(int key) = 0;

	bool				getHasPlayed(void) const;
	void				setPlayed(bool played);
	void				setNumber(int i);
	int					getNumber();
	int					getDamages();
	int					getType();
	void				move(t_coo coords);
	virtual void		getHit(int damages);
	void				kill();

protected:
	char				_symbol;
	bool 				_hasPlayed;
	t_coo				_pos;
	int					_numberOnCase;
	GameBoard			*_board;
	bool				_dead;
	void				_die(void);
	int					_life;
	int					_damages;
	int					_type;
	int					_velocity;
	int					_frames;

	virtual void		_move(void) = 0;
	virtual void		_manageHits(t_coo next);
};

#endif

