#ifndef __BASIC_H_
 #define __BASIC_H_

#include "AEnemy.class.hpp"

class Basic : public AEnemy {
public:
    Basic(void);
    Basic(GameBoard *board, const t_coo start);
    Basic(Basic const & src);
    ~Basic(void);
    
    Basic & operator=(Basic const & rhs);

    void        update(int key);
    void        shoot();
private:
    void        _move(void);
};


#endif
