#ifndef __PROJECTILE_CLASS_H_
 #define __PROJECTILE_CLASS_H_

#include "AEntity.class.hpp"

class Projectile : public AEntity{
public:
    Projectile(void);
    Projectile(GameBoard *board, const char c, const t_coo start, bool fromPlayer, int damages, int velocity);
    Projectile(Projectile const & src);
    virtual ~Projectile(void);
    Projectile & operator=(Projectile const & rhs);

    virtual void        update(int key);

protected:
    bool            _fromPlayer;
    virtual void    _move(void);
    void            getHit(int damages);
    void            _manageHits(t_coo next);
};


#endif
