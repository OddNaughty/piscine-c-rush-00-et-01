#ifndef __GAME_ENTITY_H_
 #define __GAME_ENTITY_H_

#include "GeneralHeader.hpp"

class GameEntity {
public:
    GameEntity(void);
    GameEntity(GameEntity const &src);
    ~GameEntity(void);

    GameEntity &operator=(GameEntity const &rhs);

    void    initialize();
    void    start();

    unsigned int    get_life() const;
    unsigned int    get_framerate() const;
    t_coo           get_GameSize() const;


private:
    unsigned int     _life;
    unsigned int     _framerate;
    t_coo            _GameSize;

};


#endif
