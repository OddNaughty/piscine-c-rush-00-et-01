#include "Rusher.hpp"
#include "GameBoard.class.hpp"

Rusher::Rusher(GameBoard *board, const t_coo start)
        : AEnemy(board, 'V', start, 20, 1, 1, 5) {}

Rusher::~Rusher(void){}

void    Rusher::update(int key) {
    AEnemy::update(key);
}

void    Rusher::_move(void) {
    if (!this->_hasPlayed) {
        t_coo sizeMap = this->_board->getSize();
        t_coo newCoords;
        t_coo oldCoords;
        newCoords = oldCoords = this->getPos();

        int newPosy = newCoords.y <  sizeMap.y - 1? newCoords.y + 1 : newCoords.y;

        newCoords.y = newPosy;
        if (newPosy == (int)sizeMap.y - 1) {
            this->_board->deleteFromCase(oldCoords, this);
            if (newCoords.y == oldCoords.y){
                return _die();
            }
        }
        _manageHits(newCoords);
        if (_dead)
            return _die();
        _pos = newCoords;
        this->setPlayed(true);
        this->_board->deleteFromCase(oldCoords, this);
        this->_board->addToCase(newCoords, this);
    }
}