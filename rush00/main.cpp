#include <iostream>
#include "GameEntity.hpp"
#include <ncurses.h>

int main()
{
    initscr();
    clear();
    noecho();
    cbreak();
    keypad(stdscr, TRUE);
    curs_set(0);

    GameEntity  Game;

    Game.initialize();
    Game.start();
    endwin();
    return 0;
}
