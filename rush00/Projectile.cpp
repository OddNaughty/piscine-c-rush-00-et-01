#include "Projectile.hpp"
#include "GameBoard.class.hpp"

Projectile::Projectile(GameBoard *board, const char c, const t_coo start, bool fromPlayer, int damages, int velocity)
        : AEntity::AEntity(board, c, start, 1, damages, PROJECTILE, velocity), _fromPlayer(fromPlayer) {
}

Projectile::~Projectile(void){}

void    Projectile::update(int key) {
    if (_frames == FPF)
        _frames = 0;
    _frames++;
    (void) key;
    if (_dead)
        return _die();
    _move();
    if (_dead)
        return _die();
}

void    Projectile::getHit(int damages) {
    AEntity::getHit(damages);
}

void    Projectile::_manageHits(t_coo next) {
    t_coo size =  this->_board->getCoo();
    for (int i = 0; i < ENEMY_PER_CASE; ++i) {
        if ((next.y < size.y) && this->_board->getMap()[next.y][next.x][i] != NULL && this->_board->getMap()[next.y][next.x][i]->getType() != PROJECTILE) {
            this->_board->getMap()[next.y][next.x][i]->getHit(_damages);
            this->getHit(this->_board->getMap()[next.y][next.x][i]->getDamages());
            if (_dead) {
                return;
            }
        }
    }
}

void    Projectile::_move() {
    int newPosy;
    if (!this->_hasPlayed) {
        t_coo sizeMap = this->_board->getSize();
        t_coo newCoords;
        t_coo oldCoords;
        newCoords = oldCoords = this->getPos();

        if (_fromPlayer)
            newPosy = newCoords.y >  0 ? newCoords.y - 1 : newCoords.y;
        else
            newPosy = newCoords.y <  sizeMap.y - 1 ? newCoords.y + 1 : newCoords.y;
        newCoords.y = newPosy;
        if (newPosy == 0) {
            this->_board->deleteFromCase(oldCoords, this);
            if (newCoords.y == oldCoords.y){
                return _die();
            }
        }
        _manageHits(newCoords);
        if (_dead)
            return _die();
        _pos = newCoords;
        this->setPlayed(true);
        this->_board->deleteFromCase(oldCoords, this);
        this->_board->addToCase(newCoords, this);
    }
}