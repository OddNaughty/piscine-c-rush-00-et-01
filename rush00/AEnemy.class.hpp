#ifndef ENEMY_CLASS_HPP
 #define ENEMY_CLASS_HPP

#include "GeneralHeader.hpp"
#include "AEntity.class.hpp"
#include <ncurses.h>

class AEnemy : public AEntity{
public:
	AEnemy(void);
	AEnemy(GameBoard *board, const char c, const t_coo start, unsigned int score, int life, unsigned int damages, int velocity);
	AEnemy(const AEnemy & src);
	virtual ~AEnemy(void);
	AEnemy &operator=(AEnemy const &rhs);

	virtual void		update(int key);
	void				getHit(int damages);


protected:
	virtual void	_move(void);
	unsigned int	_scoreWorth;
	void			_manageHits(t_coo next);
};

#endif