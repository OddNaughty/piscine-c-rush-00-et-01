#include "GameEntity.hpp"
#include "GameBoard.class.hpp"
#include <time.h>
#include <cstdlib>
#include <iostream>
#include <stdlib.h>
//##############################################################################
// Constructors / Destructors ##################################################

GameEntity::GameEntity(void) {
}

GameEntity::GameEntity(GameEntity const & src) {
    *this = src;
    return ;
}

GameEntity::~GameEntity(void) {
}



//##############################################################################
// Operators ###################################################################
GameEntity & GameEntity::operator=(const GameEntity  & rhs) {
    if (this != &rhs) {
        _GameSize = rhs.get_GameSize();
        _life = rhs.get_life();
        _framerate = rhs.get_framerate();
    }
    return *this;
}


//##############################################################################
// Getters/Setters #############################################################
unsigned int    GameEntity::get_life() const {
    return _life;
}

unsigned int    GameEntity::get_framerate() const {
    return _framerate;
}

t_coo           GameEntity::get_GameSize() const{
    return _GameSize;
}


//##############################################################################
// Publics functions ###########################################################

void    GameEntity::initialize() {
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

    t_coo coords = {w.ws_col, w.ws_row};
//    t_coo coords = {10, 30};
    coords.y -= 2;
    _life = 3;
    _framerate = 20;
    _GameSize = coords;
}

void    GameEntity::start() {
    //SRAND TO RANDOMLY GENERATE ENEMIES
    srand(time(NULL));
    int ch;
    clock_t start = clock();
    int     i = 0;
    clear();
    mvprintw(1, 2,"WELCOME");
    mvprintw(2, 2,"CHOOSE A DIFFICULTY");
    mvprintw(3, 2,"1 - EASY");
    mvprintw(4, 2,"2 - NORMAL");
    mvprintw(5, 2,"3 - HARD");
    mvprintw(6, 2,"4 - TEST");
    ch = getch();
    if (ch == 1 + '0')
        _life= 5;
    else if (ch == 2 + '0')
        _life= 2;
    else if (ch == 3 + '0') {
        _life = 1;
        _framerate *= 2;
    }
    else
        _life = 50;
    refresh();
    sleep(1);

    GameBoard   board(_life, _GameSize);

//MAIN LOOOOOOOOOP
    nodelay(stdscr, TRUE);
    while (1)
    {
//        timeout(0);
        if ((float)clock() - start > (float)(CLOCKS_PER_SEC / _framerate)) {
            (void)i;
            board.resetHasPlayed();
            start = clock();
            timeout(0);
            ch = getch();
            board.update(ch);
            flushinp();
            board.display();
            i++;
//            if (i > 10)
//                break;
        }
    }
    clear();
//    mvaddch(row, col, main_char);
    refresh();
}

//##############################################################################
// Private functions ###########################################################