#include "GTKUser.hpp"
#include <iostream>

GTKUser::GTKUser(GtkWidget * ptr) : ANGTKDisplayModules::ANGTKDisplayModules(ptr){
	_hostname = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mybox), _hostname);

}

void    GTKUser::display(VSTRING datas) {
	std::string combined = datas[0] + "\n" + datas[1];
	gtk_label_set_text(GTK_LABEL(_hostname), combined.c_str());
	gtk_widget_show_all(mybox);
}

GTKUser::~GTKUser(void){}
