#include "GTKTime.Class.hpp"
#include <iostream>

GTKTime::GTKTime(GtkWidget * ptr) : ANGTKDisplayModules::ANGTKDisplayModules(ptr){
	_infos = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mybox), _infos);
}

void    GTKTime::display(VSTRING datas) {
	gtk_label_set_text(GTK_LABEL(_infos), datas[0].c_str());
	gtk_widget_show_all(mybox);
}

GTKTime::~GTKTime(void){}
