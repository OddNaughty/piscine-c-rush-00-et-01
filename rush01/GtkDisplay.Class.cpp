#include "ModulesController.hpp"
#include "GtkDisplay.Class.hpp"
#include "GTKUser.hpp"
#include "GTKOs.Class.hpp"
#include "GTKTime.Class.hpp"
#include "GTKCpu.Class.hpp"
#include "GTKRam.Class.hpp"
#include "GTKNetwork.hpp"
#include <sys/ioctl.h>
#include <stdio.h>

GtkDisplay::GtkDisplay(ModulesController & existing) : _mods(existing){

//	A l'instanciation on ajoute les modules prédéfinis. TODO: Bien instantier ce qu'on veut :).


//	GtkWidget *pButton[8];
	GtkWidget *pWindow;
	GtkWidget *pVBox; // Main Box
	gtk_init(0, NULL);
	pWindow = gtk_window_new(GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title(GTK_WINDOW(pWindow), "Les GtkBox");
	gtk_window_set_default_size(GTK_WINDOW(pWindow), 800, 600);
	g_signal_connect(G_OBJECT(pWindow), "destroy", G_CALLBACK(gtk_main_quit), NULL);
	pVBox = gtk_vbox_new(TRUE, 1);/* Création de la GtkBox verticale */
	gtk_container_add(GTK_CONTAINER(pWindow), pVBox);/* Ajout de la GtkVBox dans la fenetre */




	//##################### Fin de l'initialisation ###############################

	GtkWidget *userBox;
	GtkWidget *cpuBox;
	GtkWidget *osBox;
	GtkWidget *timeBox;
	GtkWidget *ramBox;
	GtkWidget *networkBox;
	(void)userBox;(void)cpuBox;(void)osBox;(void)timeBox;(void)ramBox;
	for (unsigned int i = 0; i < _mods._currents.size(); ++i) {
		if (_mods._currents[i]->getType() == "UserModule") {
			userBox = gtk_hbox_new(TRUE, 0);
			gtk_box_pack_start(GTK_BOX(pVBox), userBox, FALSE, FALSE, 0);
			_displayClass[_mods._currents[i]] = new GTKUser(userBox);
		}
		else if (_mods._currents[i]->getType() == "OSInfoModule"){
			osBox = gtk_hbox_new(TRUE, 0);
			gtk_box_pack_start(GTK_BOX(pVBox), osBox, FALSE, FALSE, 0);
			_displayClass[_mods._currents[i]] = new GTKOs(osBox);
		}
		else if (_mods._currents[i]->getType() == "TimeModule"){
			timeBox = gtk_hbox_new(TRUE, 0);
			gtk_box_pack_start(GTK_BOX(pVBox), timeBox, FALSE, FALSE, 0);
			_displayClass[_mods._currents[i]] = new GTKTime(timeBox);
	}
		else if (_mods._currents[i]->getType() == "CPUModule"){
			cpuBox = gtk_hbox_new(TRUE, 0);
			gtk_box_pack_start(GTK_BOX(pVBox), cpuBox, FALSE, FALSE, 0);
			_displayClass[_mods._currents[i]] = new GTKCpu(cpuBox);
		}
		else if (_mods._currents[i]->getType() == "RAMModule"){
			ramBox = gtk_hbox_new(TRUE, 0);
			gtk_box_pack_start(GTK_BOX(pVBox), ramBox, FALSE, FALSE, 0);
			_displayClass[_mods._currents[i]] = new GTKRam(ramBox);
		}
		else if (_mods._currents[i]->getType() == "NetworkModule"){
			networkBox = gtk_hbox_new(TRUE, 0);
			gtk_box_pack_start(GTK_BOX(pVBox), networkBox, FALSE, FALSE, 0);
			_displayClass[_mods._currents[i]] = new GTKNetwork(networkBox);
		}
	}

	gtk_widget_show_all(pWindow);
	g_timeout_add(1000, (GSourceFunc)GtkDisplay::beforeDisplayModules, this);
	gtk_main();
}

GtkDisplay::~GtkDisplay(void) {
}

void				GtkDisplay::start(void){
}
void				GtkDisplay::end(){
}
void				GtkDisplay::addModuleMenu(void){
}
void				GtkDisplay::removeModuleMenu(void){
}

int	 GtkDisplay::beforeDisplayModules(GtkDisplay *display){
	display->displayModules();
	return true;
}

int					GtkDisplay::displayModules(void){
	VSTRING			datas;

	for(unsigned int i = 0; i != _mods._currents.size(); i++) {
		datas = _mods._currents[i]->getDatas();
		if (_displayClass.count(_mods._currents[i]))
			_displayClass[_mods._currents[i]]->display(datas);
	}
	return true;
}
void				GtkDisplay::addModule(std::string type){
	(void)type;
}
void				GtkDisplay::removeModule(std::string type){
	(void)type;
}