#include <algorithm>
#include <iostream>
#include "ModulesController.hpp"
#include "UserModule.hpp"
#include "TimeModule.hpp"
#include "OSInfoModule.hpp"
#include "CPUModule.hpp"
#include "RAMModule.hpp"
#include "NetworkModule.hpp"

ModulesController::ModulesController(void) {}
ModulesController::~ModulesController(void) {}

int		ModulesController::addModule(std::string type) {
	for (unsigned int i = 0; i < _currents.size(); ++i) {
		if (_currents[i]->getType() == type)
			return -1;
	}
	if (type == "UserModule") {
		_currents.push_back(new UserModule());
		return 0;
	}
	else if (type == "OSInfoModule") {
		_currents.push_back(new OSInfoModule());
		return 1;
	}
	else if (type == "TimeModule") {
		_currents.push_back(new TimeModule());
		return 2;
	}
	else if (type == "CPUModule") {
		_currents.push_back(new CPUModule());
		return 3;
	}
	else if (type == "RAMModule") {
		_currents.push_back(new RAMModule());
		return 4;
	}
	else if (type == "NetworkModule") {
		_currents.push_back(new NetworkModule());
		return 5;
	}
	return -1;
}

void	ModulesController::removeModule(std::string type) {
	std::cout << "LoLoL i'm in here and i wanna delete " << type << std::endl;
}

void	ModulesController::displayInfos(void) {
	VSTRING		datas;
	for(int i = 0; i != (int) _currents.size(); i++) {
		datas = _currents[i]->getDatas();
		for (int j = 0; j < (int) datas.size(); ++j) {
			std::cout << datas[j] << std::endl;
		}
	}
}