#include "GTKNetwork.hpp"
#include <iostream>

GTKNetwork::GTKNetwork(GtkWidget * ptr) : ANGTKDisplayModules::ANGTKDisplayModules(ptr){
	_hostname = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mybox), _hostname);

}

void    GTKNetwork::display(VSTRING datas) {
	std::string combined = datas[0] + "/" + datas[1];
	gtk_label_set_text(GTK_LABEL(_hostname), combined.c_str());
	gtk_widget_show_all(mybox);
}

GTKNetwork::~GTKNetwork(void){}
