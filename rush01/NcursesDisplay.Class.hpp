#ifndef NCURSESDISPLAY_HPP
# define NCURSESDISPLAY_HPP

#include "IMonitorDisplay.Class.hpp"
#include <stdlib.h>
#include <map>
class ModulesController;
class ANcursesDisplayModules;
class IMonitorModule;
typedef std::map<IMonitorModule*, ANcursesDisplayModules*> LINK_MAP;

typedef struct s_coo {
	int			x;
	int			y;
}				t_coo;

class NcursesDisplay : public IMonitorDisplay {


public:
	NcursesDisplay( ModulesController & existing );
	~NcursesDisplay( void );
	void				start(void);
	void				end();
	void				addModuleMenu(void);
	void				removeModuleMenu(void);
	int					displayModules(void);
	void				addModule(std::string type);
	void				removeModule(std::string type);

protected:
	t_coo				windowSize;
private:
	NcursesDisplay(void);
	NcursesDisplay ( NcursesDisplay const & src );
	NcursesDisplay	&operator=( NcursesDisplay const & rhs );
	ModulesController	&_mods;
	LINK_MAP 			_displayClass;
	WINDOW				*bigwindow;
};

#endif