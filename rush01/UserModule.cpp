#include <sys/sysctl.h>
#include <cstdlib>
#include "AMonitorModule.hpp"
#include "UserModule.hpp"


UserModule::UserModule(void) : AMonitorModule::AMonitorModule("UserModule") {}


VSTRING			UserModule::getDatas() {
	VSTRING infos;
	char buf[1024];
	size_t size = sizeof(buf);
	sysctlbyname("kern.hostname", &buf, &size, NULL, 0);
	infos.push_back(buf);
	infos.push_back(getenv("USER"));
	return infos;
}