#ifndef __NCURSES_NETWORK_H_
#define __NCURSES_NETWORK_H_

#include "ANcursesDisplayModule.hpp"

class NcursesNetwork : public ANcursesDisplayModules {
private:
    NcursesNetwork(NcursesNetwork const &src);
    NcursesNetwork &operator=(NcursesNetwork const &rhs);

public:
    NcursesNetwork();
    ~NcursesNetwork(void);
    virtual	void		display(VSTRING datas);
};



#endif
