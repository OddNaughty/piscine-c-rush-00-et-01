#ifndef GTKDISPLAY_HPP
# define GTKDISPLAY_HPP

#include "IMonitorDisplay.Class.hpp"
#include "IMonitorModule.hpp"
#include <map>
#include <stdlib.h>
#include <gtk/gtk.h>
#include <unistd.h>
class ModulesController;
class ANGTKDisplayModules;
class IMonitorModule;
typedef std::map<IMonitorModule*, ANGTKDisplayModules*> LINK_MAP_GTK;

class GtkDisplay : public IMonitorDisplay {

public:
	GtkDisplay ( ModulesController & existing );
	~GtkDisplay( void );
	void				start(void);
	void				end();
	void				addModuleMenu(void);
	void				removeModuleMenu(void);
	int					displayModules(void);
	void				addModule(std::string type);
	void				removeModule(std::string type);
	static int			beforeDisplayModules(GtkDisplay *display);

private:
	GtkDisplay ( void );
	GtkDisplay ( GtkDisplay const & src );
	GtkDisplay	&operator=( GtkDisplay const & rhs );
	ModulesController	&_mods;
	LINK_MAP_GTK 			_displayClass;
};

#endif