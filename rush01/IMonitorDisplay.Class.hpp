#ifndef IMONITORDISPLAY_HPP
# define IMONITORDISPLAY_HPP

#include <ncurses.h>
#include <iostream>
#include <unistd.h>

class IMonitorDisplay {
public:
	virtual ~IMonitorDisplay() {};
	virtual void		start(void) = 0;
	virtual void		end(void) = 0;
	virtual void		addModuleMenu(void) = 0;
	virtual	void		addModule(std::string type) = 0;
	virtual void		removeModuleMenu(void) = 0;
	virtual	void		removeModule(std::string type) = 0;
	virtual int			displayModules(void) = 0;
};

#endif