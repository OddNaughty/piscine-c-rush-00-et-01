#include <iostream>
#include <unistd.h>
#include "NcursesTime.hpp"

NcursesTime::NcursesTime(void) : ANcursesDisplayModules(10, 50, 0, 50) {
}

NcursesTime::~NcursesTime() {}

void NcursesTime::display(VSTRING datas) {
    wclear(window);
    const std::string name = "Time";
    mvwprintw(window, 2, 0, "==================================================");
    mvwprintw(window, 1, (50 / 2) - (sizeof(name.c_str()) / 2),  name.c_str());
    int     lines = 2;

    for (int j = 0; j < (int) datas.size(); ++j) {
        mvwprintw(window, lines + 1, 2, datas[j].c_str());
        lines++;
    }
    box(window, 0, 0);
    wrefresh(window);
}