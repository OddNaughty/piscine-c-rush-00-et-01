#include <mach/mach_init.h>
#include <mach/mach_error.h>
#include <mach/mach_host.h>
#include <mach/vm_map.h>
#include <sys/sysctl.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <string.h>
#include "AMonitorModule.hpp"
#include "CPUModule.hpp"


CPUModule::CPUModule(void) : AMonitorModule::AMonitorModule("CPUModule") {}

double CPUModule::GetCPULoad()
{
    host_cpu_load_info_data_t cpuinfo;
    mach_msg_type_number_t count = HOST_CPU_LOAD_INFO_COUNT;
    if (host_statistics(mach_host_self(), HOST_CPU_LOAD_INFO, (host_info_t)&cpuinfo, &count) == KERN_SUCCESS)
    {
        unsigned long long totalTicks = 0;
        for(int i=0; i<CPU_STATE_MAX; i++) totalTicks += cpuinfo.cpu_ticks[i];
        return CalculateCPULoad(cpuinfo.cpu_ticks[CPU_STATE_IDLE], totalTicks);
    }
    else return -1.0f;
}
//
//std::string CPUModule::GetStdoutFromCommand(std::string cmd) {
//    std::string data;
//    FILE * stream;
//    const int max_buffer = 256;
//    char buffer[max_buffer];
//    cmd.append(" 2>&1");
//
//    stream = popen(cmd.c_str(), "r");
//    if (stream) {
//        while (!feof(stream))
//            if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
//        pclose(stream);
//    }
//    return data;
//}


double CPUModule::CalculateCPULoad(unsigned long long idleTicks, unsigned long long totalTicks)
{
    static unsigned long long _previousTotalTicks = 0;
    static unsigned long long _previousIdleTicks = 0 ;
    unsigned long long totalTicksSinceLastTime = totalTicks-_previousTotalTicks;
    unsigned long long idleTicksSinceLastTime  = idleTicks-_previousIdleTicks;
    double ret = 1.0f-((totalTicksSinceLastTime > 0) ? ((float)idleTicksSinceLastTime)/totalTicksSinceLastTime : 0);
    _previousTotalTicks = totalTicks;
    _previousIdleTicks  = idleTicks;
    return ret;

}




VSTRING			CPUModule::getDatas() {
    VSTRING infos;
    double dbl = GetCPULoad() * 100;
    char buf[1024];
    int  cpucore;
    uint64_t freq = 0;
    size_t sizeFreq = sizeof(freq);
    std::ostringstream strs;

    strs << dbl;
    std::string str = strs.str();
    infos.push_back(str);
    strs.clear();//clear any bits set
    strs.str(std::string());
    size_t size = sizeof(buf);
    size_t sizeCore = sizeof(cpucore);
    sysctlbyname("hw.ncpu", &cpucore, &sizeCore, NULL, 0);
    strs << cpucore;
    str = strs.str();
    infos.push_back(str);
    sysctlbyname("machdep.cpu.brand_string", &buf, &size, NULL, 0);
    infos.push_back(buf);
    strs.clear();//clear any bits set
    strs.str(std::string());
    sysctlbyname("hw.cpufrequency", &freq, &sizeFreq, NULL, 0);
    strs << freq;
    infos.push_back(strs.str());
    return infos;
}