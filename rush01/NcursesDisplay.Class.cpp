#include "NcursesDisplay.Class.hpp"
#include "UserModule.hpp"
#include "ModulesController.hpp"
#include "NcursesUserInfo.hpp"
#include "NcursesOSInfo.hpp"
#include "NcursesTime.hpp"
#include "NcursesCPU.hpp"
#include "NcursesRAM.hpp"
#include "NcursesNetwork.hpp"
#include <sys/ioctl.h>
#include <stdio.h>



NcursesDisplay::NcursesDisplay(ModulesController & existing) : _mods(existing){
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

	t_coo size = {w.ws_col, w.ws_row};
	windowSize = size;
	initscr();
	clear();
	noecho();
	cbreak();
	keypad(stdscr, TRUE);
	curs_set(FALSE);
	bigwindow = newwin(size.y - 20 , size.x, size.y - 20, 0);
	box(bigwindow, 0, 0);
	wrefresh(bigwindow);
	wtimeout(bigwindow, 1000);
//	A l'instanciation on ajoute les modules prédéfinis. TODO: Bien instantier ce qu'on veut :).
	for (unsigned int i = 0; i < _mods._currents.size(); ++i) {
		if (_mods._currents[i]->getType() == "UserModule")
			_displayClass[_mods._currents[i]] = new NcursesUserInfo();
		else if (_mods._currents[i]->getType() == "OSInfoModule")
			_displayClass[_mods._currents[i]] = new NcursesOSInfo();
		else if (_mods._currents[i]->getType() == "TimeModule")
			_displayClass[_mods._currents[i]] = new NcursesTime();
		else if (_mods._currents[i]->getType() == "CPUModule")
			_displayClass[_mods._currents[i]] = new NcursesCPU();
		else if (_mods._currents[i]->getType() == "RAMModule")
			_displayClass[_mods._currents[i]] = new NcursesRAM();
		else if (_mods._currents[i]->getType() == "NetworkModule")
			_displayClass[_mods._currents[i]] = new NcursesNetwork();
	}
}

NcursesDisplay::~NcursesDisplay(void) {
	endwin();
}

// La classe qui va servir a getDatas() tout, elle est #swag

int 	NcursesDisplay::displayModules(void) {
	VSTRING			datas;
	int			ch;

	for(unsigned int i = 0; i != _mods._currents.size(); i++) {
		datas = _mods._currents[i]->getDatas();
		if (_displayClass.count(_mods._currents[i]))
			_displayClass[_mods._currents[i]]->display(datas);
	}
	mvwprintw(bigwindow, 1, 1, "Appuez sur A pour ajouter ou B pour le supprimer");
	ch = wgetch(bigwindow);
	return ch;
}



void	NcursesDisplay::start(void){
	int ch;
	wclear(bigwindow);
	mvwprintw(bigwindow, 1, 2,"WELCOME");
	box(bigwindow, 0, 0);
	wrefresh(bigwindow);
	ch = wgetch(bigwindow);
	wclear(bigwindow);
	box(bigwindow, 0, 0);
	wrefresh(bigwindow);
	while (1)
	{
		ch = displayModules();
		if (ch == 'A')
			this->addModuleMenu();
		else if (ch == 'B')
			this->removeModuleMenu();
		else if (ch == 27)
			end();
		box(bigwindow, 0, 0);
		wrefresh(bigwindow);
	}
}

void	NcursesDisplay::end() {
	endwin();
	exit (0);
}

void		NcursesDisplay::addModuleMenu(void){

	wclear(bigwindow);
	int ch;
	while (1) {
		mvwprintw(bigwindow,0, 2,"************************");
		mvwprintw(bigwindow,1, 2,"Press a key:");
		mvwprintw(bigwindow,2, 2,"1 - Add UserModule");
		mvwprintw(bigwindow,3, 2,"2 - Add OSInfoModule");
		mvwprintw(bigwindow,4, 2,"3 - TimeModule");
		mvwprintw(bigwindow,5, 2,"4 - CPUModule");
		mvwprintw(bigwindow,6, 2,"5 - RAMModule");
		mvwprintw(bigwindow,7, 2,"6 - RAMModule");
		mvwprintw(bigwindow,8, 2,"Q - Return to monitoring");
		ch = displayModules();
		wclear(bigwindow);
		wrefresh(bigwindow);
		if (ch == 'Q') {
			wclear(bigwindow);
			wrefresh(bigwindow);
			return;
		}
		else if (ch == '1')
			return addModule("UserModule");
		else if (ch == '2')
			return addModule("OSInfoModule");
		else if (ch == '3')
			return addModule("TimeModule");
		else if (ch == '4')
			return addModule("CPUModule");
		else if (ch == '5')
			return addModule("RAMModule");
		else if (ch == '6')
			return addModule("NetworkModule");
	}
}

void		NcursesDisplay::addModule(std::string type) {
	int whichClass = _mods.addModule(type);
	if (whichClass == -1)
		return ;
	AMonitorModule *where = _mods._currents.back();
	switch (whichClass) {
		case 0:
			_displayClass[where] = new NcursesUserInfo();
		break;
		case 1:
			_displayClass[where] = new NcursesOSInfo();
		break;
		case 2:
			_displayClass[where] = new NcursesTime();
		break;
		case 3:
			_displayClass[where] = new NcursesCPU();
			break;
		case 4:
			_displayClass[where] = new NcursesRAM();
			break;
		case 5:
			_displayClass[where] = new NcursesNetwork();
			break;
	}
}

void		NcursesDisplay::removeModuleMenu(void){
	wclear(bigwindow);
	int ch;
	while (1){
		mvwprintw(bigwindow, 0, 2,"************************");
		mvwprintw(bigwindow, 1, 2,"Press a key:");
		mvwprintw(bigwindow, 2, 2,"1 - Remove UserModule");
		mvwprintw(bigwindow, 3, 2,"2 - Remove OSInfoModule");
		mvwprintw(bigwindow, 4, 2,"3 - Remove TimeModule");
		mvwprintw(bigwindow, 5, 2,"4 - Remove CPUModule");
		mvwprintw(bigwindow, 6, 2,"5 - Remove RAMModule");
		mvwprintw(bigwindow, 7, 2,"6 - Remove NetworkModule");
		mvwprintw(bigwindow, 8, 2,"Q - Return to monitoring");
		ch = displayModules();
		wclear(bigwindow);
		wrefresh(bigwindow);
		if (ch == 'Q') {
			wclear(bigwindow);
			wrefresh(bigwindow);
			return;
		}
		else if (ch == '1')
			return removeModule("UserModule");
		else if (ch == '2')
			return removeModule("OSInfoModule");
		else if (ch == '3')
			return removeModule("TimeModule");
		else if (ch == '4')
			return removeModule("CPUModule");
		else if (ch == '5')
			return removeModule("RAMModule");
		else if (ch == '6')
			return removeModule("NetworkModule");
	}
}

void		NcursesDisplay::removeModule(std::string type) {
	VMOD ::iterator it = _mods._currents.begin();
	while (it != _mods._currents.end()) {
		if ((*it)->getType() == type)
			break ;
		it++;
	}
	if (it == _mods._currents.end()) {
		return ;
	}
	_displayClass[*it]->end();
	_displayClass.erase(*it);
	_mods._currents.erase(it);
	wclear(bigwindow);
	wrefresh(bigwindow);
}