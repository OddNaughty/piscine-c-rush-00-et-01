#ifndef __NCURSES_DISPLAY_MODULE_H_
#define __NCURSES_DISPLAY_MODULE_H_

#include <ncurses.h>
#include <vector>
#include <string>

typedef std::vector<std::string> VSTRING;


class ANcursesDisplayModules {
public:
	ANcursesDisplayModules(int height, int width, int starty, int startx);
	virtual ~ANcursesDisplayModules(void) {};
	virtual	void		display(VSTRING) = 0;
	void		end();
protected:
	WINDOW		*window;
private:
	ANcursesDisplayModules(void);
	ANcursesDisplayModules(ANcursesDisplayModules const &src);
	ANcursesDisplayModules &operator=(ANcursesDisplayModules const &rhs);

};


#endif
