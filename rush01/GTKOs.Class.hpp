#ifndef GTKOS_HPP
# define GTKOS_HPP

#include <gtk/gtk.h>
#include "ANGTKDisplayModules.hpp"

class GTKOs : public ANGTKDisplayModules{
public:
	GTKOs(GtkWidget *ptr);
	~GTKOs(void);
	virtual void    display(VSTRING datas);
private:
	GTKOs(void);
	GTKOs(GTKOs const &src);
	GTKOs &operator=(GTKOs const &rhs);
	GtkWidget * _infos;
};

#endif