#ifndef __MODULES_CONTROLLER_H_
#define __MODULES_CONTROLLER_H_

#include "ft_gkrellm.hpp"
#include "AMonitorModule.hpp"

typedef std::vector<AMonitorModule*>	VMOD;

class ModulesController {
public:
	ModulesController(void);
	~ModulesController(void);

	/// Il faut tester les 3 conneries !
	int		addModule(std::string type);
	void	removeModule(std::string type);
	void	displayInfos(void);
	VMOD	_currents;

private:
	ModulesController(ModulesController const &src);
	ModulesController &operator=(ModulesController const &rhs);

};


#endif
