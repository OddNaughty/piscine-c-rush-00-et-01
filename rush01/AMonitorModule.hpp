#ifndef __AMONITOR_MODULE_H_
#define __AMONITOR_MODULE_H_

#include "IMonitorModule.hpp"

class AMonitorModule : public IMonitorModule {
public:
    AMonitorModule(std::string type);
    ~AMonitorModule(void);
    virtual VSTRING	getDatas(void) = 0;
    std::string 	getType(void);

private:
    std::string         _type;
    AMonitorModule(void);
    AMonitorModule(AMonitorModule const &src);
    AMonitorModule &operator=(AMonitorModule const &rhs);
};


#endif
