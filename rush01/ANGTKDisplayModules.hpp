#ifndef __ANGTK_DISPLAY_MODULES_H_
#define __ANGTK_DISPLAY_MODULES_H_

#include <string>
#include <gtk/gtk.h>
#include <vector>

typedef std::vector<std::string> VSTRING;

class ANGTKDisplayModules {
public:
    ANGTKDisplayModules(GtkWidget *ptr);
    virtual ~ANGTKDisplayModules(void);
    virtual void    display(VSTRING datas) = 0;
protected:
    GtkWidget       *mybox;
private:
    ANGTKDisplayModules(void);
    ANGTKDisplayModules(ANGTKDisplayModules const &src);
    ANGTKDisplayModules &operator=(ANGTKDisplayModules const &rhs);
};


#endif
