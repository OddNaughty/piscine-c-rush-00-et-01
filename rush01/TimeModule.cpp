#include <ctime>
#include "AMonitorModule.hpp"
#include "TimeModule.hpp"

TimeModule::TimeModule(void) : AMonitorModule::AMonitorModule("TimeModule") {}

VSTRING			TimeModule::getDatas() {
    VSTRING infos;

    time_t rawtime;
    struct tm * timeinfo;
    char buffer [128];

    time (&rawtime);
    timeinfo = localtime (&rawtime);

    strftime (buffer,128,"It's %X.",timeinfo);
    infos.push_back(buffer);
    return infos;
}