#include <sys/sysctl.h>
#include <sys/types.h>
#include <ctime>
#include <stdlib.h>
#include <string.h>
#include <mach/vm_statistics.h>
#include <mach/mach_types.h>
#include <mach/mach_init.h>
#include <mach/mach_host.h>
#include <iostream>
#include <sstream>
#include <stdio.h>
#include "AMonitorModule.hpp"
#include "RAMModule.hpp"

RAMModule::RAMModule(void) : AMonitorModule::AMonitorModule("RAMModule") {}

double RAMModule::ParseMemValue(const char * b)
{
    while((*b)&&(isdigit(*b) == false)) b++;
    return isdigit(*b) ? atof(b) : -1.0;
}

//Returns a number between 0.0f and 1.0f, with 0.0f meaning all RAM is available, and 1.0f meaning all RAM is currently in use
float RAMModule::GetSystemMemoryUsagePercentage()
{
    FILE * fpIn = popen("/usr/bin/vm_stat", "r");
    if (fpIn)
    {
        double pagesUsed = 0.0, totalPages = 0.0;
        char buf[512];
        while(fgets(buf, sizeof(buf), fpIn) != NULL)
        {
            if (strncmp(buf, "Pages", 5) == 0)
            {
                double val = ParseMemValue(buf);
                if (val >= 0.0)
                {
                    if ((strncmp(buf, "Pages wired", 11) == 0)||(strncmp(buf, "Pages active", 12) == 0)) pagesUsed += val;
                    totalPages += val;
                }
            }
            else if (strncmp(buf, "Mach Virtual Memory Statistics", 30) != 0) break;  // Stop at "Translation Faults", we don't care about anything at or below that
        }
        pclose(fpIn);
        if (totalPages > 0.0) return (float) (pagesUsed/totalPages);
    }
    std::cout << "Broken !!!" << std::endl;
    return -1.0f;  // indicate failure
}



VSTRING			RAMModule::getDatas() {
    /// ORDER: Percentage of ram used, free_memory and memory_used
    VSTRING infos;
    float   memoryPercentage = GetSystemMemoryUsagePercentage();
    std::ostringstream strs;

    if (memoryPercentage != -1.0f) {
        strs << memoryPercentage * 100;
        std::string str = strs.str();
        infos.push_back(str);
    }

    int mib[2];
    int64_t physical_memory;
    mib[0] = CTL_HW;
    mib[1] = HW_MEMSIZE;
    size_t length = sizeof(int64_t);
    sysctl(mib, 2, &physical_memory, &length, NULL, 0);
    strs.clear();//clear any bits set
    strs.str(std::string());
    strs << physical_memory;
    infos.push_back(strs.str());


    vm_size_t page_size;
    mach_port_t mach_port;
    mach_msg_type_number_t count;
    vm_statistics64_data_t vm_stats;

    mach_port = mach_host_self();
    count = sizeof(vm_stats) / sizeof(natural_t);
    long long free_memory;
    long long used_memory;
    if (KERN_SUCCESS == host_page_size(mach_port, &page_size) &&
            KERN_SUCCESS == host_statistics64(mach_port, HOST_VM_INFO,
                    (host_info64_t)&vm_stats, &count))
    {
        free_memory = (int64_t)vm_stats.free_count * (int64_t)page_size;

        used_memory = ((int64_t)vm_stats.active_count +
                (int64_t)vm_stats.inactive_count +
                (int64_t)vm_stats.wire_count) *  (int64_t)page_size;
    }
    strs.clear();//clear any bits set
    strs.str(std::string());
    strs << free_memory;
    infos.push_back(strs.str());
    strs.clear();//clear any bits set
    strs.str(std::string());
    strs << used_memory;
    infos.push_back(strs.str());

    return infos;
}