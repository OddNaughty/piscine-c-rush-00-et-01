#ifndef __TIME_MODULE_H_
#define __TIME_MODULE_H_

#include "AMonitorModule.hpp"

class TimeModule : public AMonitorModule {
public:
    TimeModule(void);
    ~TimeModule(void);

    VSTRING		getDatas(void);
private:
    TimeModule(TimeModule const &src);
    TimeModule &operator=(TimeModule const &rhs);

};

#endif
