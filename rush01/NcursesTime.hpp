#ifndef __NCURSES_TIME_H_
#define __NCURSES_TIME_H_

#include "ANcursesDisplayModule.hpp"

class NcursesTime : public ANcursesDisplayModules {
private:
    NcursesTime(NcursesTime const &src);
    NcursesTime &operator=(NcursesTime const &rhs);

public:
    NcursesTime();
    ~NcursesTime(void);
    virtual	void		display(VSTRING datas);
};


#endif
