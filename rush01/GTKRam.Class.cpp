#include "GTKRam.Class.hpp"
#include <iostream>

GTKRam::GTKRam(GtkWidget * ptr) : ANGTKDisplayModules::ANGTKDisplayModules(ptr){
	_infos = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mybox), _infos);
}

void    GTKRam::display(VSTRING datas) {
	std::string combined = datas[0] + "% of " + datas[1] + " bytes\n";
	std::string combined2 = combined + datas[2] + "/" + datas[3];
	gtk_label_set_text(GTK_LABEL(_infos), combined2.c_str());
	gtk_widget_show_all(mybox);
}

GTKRam::~GTKRam(void){}
