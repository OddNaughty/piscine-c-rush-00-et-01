#include <iostream>
#include <unistd.h>
#include "NcursesUserInfo.hpp"

NcursesUserInfo::NcursesUserInfo(void) : ANcursesDisplayModules(10, 50, 0, 0) {
}

NcursesUserInfo::~NcursesUserInfo() {}

void NcursesUserInfo::display(VSTRING datas) {
    wclear(window);
    const std::string name = "User Infos";
    int     lines = 2;
    mvwprintw(window, 2, 0, "==================================================");
    mvwprintw(window, 1, (50 / 2) - (sizeof(name.c_str()) / 2),  name.c_str());
    for (int j = 0; j < (int) datas.size(); ++j) {
        mvwprintw(window, lines + 1, 2, datas[j].c_str());
        lines++;
    }
    box(window, 0, 0);
    wrefresh(window);
}