#include <iostream>
#include <unistd.h>
#include "NcursesOSInfo.hpp"

NcursesOSInfo::NcursesOSInfo(void) : ANcursesDisplayModules(10, 50, 10, 0) {
}

NcursesOSInfo::~NcursesOSInfo() {}

void NcursesOSInfo::display(VSTRING datas) {
    const std::string name = "OS Infos";
    wclear(window);
    int     lines = 2;
    mvwprintw(window, 2, 0, "==================================================");
    mvwprintw(window, 1, (50 / 2) - (sizeof(name.c_str()) / 2),  name.c_str());
    if (datas.size() >= 1)
        datas[0] = "Name:\t\t" + datas[0];
    if (datas.size() >= 2)
        datas[1] = "Version:\t" + datas[1];
    for (int j = 0; j < (int) datas.size(); ++j) {
        mvwprintw(window, lines + 1, 2, datas[j].c_str());
        lines++;
    }
    box(window, 0, 0);
    wrefresh(window);
}