#ifndef __NCURSES_RAM_H_
#define __NCURSES_RAM_H_

#include "ANcursesDisplayModule.hpp"

class NcursesRAM : public ANcursesDisplayModules {
private:
    NcursesRAM(NcursesRAM const &src);
    NcursesRAM &operator=(NcursesRAM const &rhs);

public:
    NcursesRAM();
    ~NcursesRAM(void);
    virtual	void		display(VSTRING datas);
};



#endif
