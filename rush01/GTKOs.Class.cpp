#include "GTKOs.Class.hpp"
#include <iostream>

GTKOs::GTKOs(GtkWidget * ptr) : ANGTKDisplayModules::ANGTKDisplayModules(ptr){
	_infos = gtk_label_new("");
	gtk_container_add(GTK_CONTAINER(mybox), _infos);
}

void    GTKOs::display(VSTRING datas) {
	std::string combined = datas[0] + "\n" + datas[1];
	gtk_label_set_text(GTK_LABEL(_infos), combined.c_str());
	gtk_widget_show_all(mybox);
}

GTKOs::~GTKOs(void){}
