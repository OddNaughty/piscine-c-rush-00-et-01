#include "ANcursesDisplayModule.hpp"

ANcursesDisplayModules::ANcursesDisplayModules(int height, int width, int starty, int startx) {
    WINDOW *local_win;

    local_win = newwin(height, width, starty, startx);
    box(local_win, 0 , 0);
    wrefresh(local_win);
    window = local_win;
}

void        ANcursesDisplayModules::end() {
    wclear(window);
    wrefresh(window);
    delete this;
}