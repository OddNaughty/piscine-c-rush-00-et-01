#ifndef GTKCPU_HPP
# define GTKCPU_HPP

#include <gtk/gtk.h>
#include "ANGTKDisplayModules.hpp"

class GTKCpu : public ANGTKDisplayModules{
public:
	GTKCpu(GtkWidget *ptr);
	~GTKCpu(void);
	virtual void    display(VSTRING datas);
private:
	GTKCpu(void);
	GTKCpu(GTKCpu const &src);
	GTKCpu &operator=(GTKCpu const &rhs);
};

#endif