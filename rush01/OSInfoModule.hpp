#ifndef __OS_INFO_MODULE_H_
#define __OS_INFO_MODULE_H_

#include "AMonitorModule.hpp"

class OSInfoModule : public AMonitorModule {
public:
	OSInfoModule(void);
	~OSInfoModule(void);

	VSTRING		getDatas(void);
private:
	OSInfoModule(OSInfoModule const &src);
	OSInfoModule &operator=(OSInfoModule const &rhs);
};

#endif
