#include <iostream>
#include <unistd.h>
#include "NcursesNetwork.hpp"

NcursesNetwork::NcursesNetwork(void) : ANcursesDisplayModules(10, 50, 20, 50) {
}

NcursesNetwork::~NcursesNetwork() {}

void NcursesNetwork::display(VSTRING datas) {
    const std::string name = "Network Infos";
    wclear(window);
    mvwprintw(window, 2, 0, "==================================================");
    mvwprintw(window, 1, (50 / 2) - (sizeof(name.c_str()) / 2),  name.c_str());
    if (datas.size() == 2) {
        mvwprintw(window, 3, 2, datas[0].c_str());
        mvwprintw(window, 3, 2 + datas[0].length(), "/");
        mvwprintw(window, 3, 2 + datas[0].length() + 1, datas[1].c_str());
    }
    box(window, 0, 0);
    wrefresh(window);
}