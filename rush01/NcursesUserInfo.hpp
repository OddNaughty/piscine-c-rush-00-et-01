#ifndef __NCURSES_USER_INFO_H_
#define __NCURSES_USER_INFO_H_

#include "ANcursesDisplayModule.hpp"

class NcursesUserInfo : public ANcursesDisplayModules {
private:
    NcursesUserInfo(NcursesUserInfo const &src);
    NcursesUserInfo &operator=(NcursesUserInfo const &rhs);

public:
    NcursesUserInfo();
    ~NcursesUserInfo(void);
    virtual	void		display(VSTRING datas);
};


#endif
