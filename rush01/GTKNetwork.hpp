#ifndef __GTK_NETWORK_H_
#define __GTK_NETWORK_H_

#include <gtk/gtk.h>


#include "ANGTKDisplayModules.hpp"

class GTKNetwork : public ANGTKDisplayModules {
public:
    GTKNetwork(GtkWidget *ptr);
    ~GTKNetwork(void);
    virtual void    display(VSTRING datas);
private:
    GTKNetwork(void);
    GTKNetwork(GTKNetwork const &src);
    GTKNetwork &operator=(GTKNetwork const &rhs);
    GtkWidget *_hostname;
};


#endif
