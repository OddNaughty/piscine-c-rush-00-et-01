#ifndef __NETWORK_MODULE_H_
#define __NETWORK_MODULE_H_

#include "AMonitorModule.hpp"

class NetworkModule : public AMonitorModule {
public:
	NetworkModule(void);
	~NetworkModule(void);

	VSTRING		getDatas(void);
private:
	NetworkModule(NetworkModule const &src);
	NetworkModule &operator=(NetworkModule const &rhs);
};

#endif
