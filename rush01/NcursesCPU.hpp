#ifndef __NCURSES_CPU_H_
#define __NCURSES_CPU_H_

#include "ANcursesDisplayModule.hpp"

class NcursesCPU : public ANcursesDisplayModules {
private:
    NcursesCPU(NcursesCPU const &src);
    NcursesCPU &operator=(NcursesCPU const &rhs);

public:
    NcursesCPU();
    ~NcursesCPU(void);
    virtual	void		display(VSTRING datas);
};



#endif
