#include "ft_gkrellm.hpp"
#include "ModulesController.hpp"
#include <stdlib.h>
//#include <gtk/gtk.h>
#include <iostream>
#include "NcursesDisplay.Class.hpp"
#include "GtkDisplay.Class.hpp"


int main(int ac, char **av)
{

	ModulesController ctrl;

	ctrl.addModule("UserModule");
	ctrl.addModule("OSInfoModule");
	ctrl.addModule("TimeModule");
	ctrl.addModule("CPUModule");
	ctrl.addModule("RAMModule");
	ctrl.addModule("NetworkModule");
	if (ac != 2){
		std::cout << "Usage: \t-g:\tgraphic mode" << std::endl << 	"\t-t:\tterminal mode"<<std::endl;
		return 1;
	}
	std::string arg = av[1];
	if (arg == "-t") {
		NcursesDisplay	monitou(ctrl);
		monitou.start();
	}
	else if (arg == "-g"){
		GtkDisplay gtkou(ctrl);

	}
	else {
		ctrl.displayInfos();
		return 1;
	}
	return 0;
}