#ifndef GTKRAM_HPP
# define GTKRAM_HPP

#include <gtk/gtk.h>
#include "ANGTKDisplayModules.hpp"

class GTKRam : public ANGTKDisplayModules{
public:
	GTKRam(GtkWidget *ptr);
	~GTKRam(void);
	virtual void    display(VSTRING datas);
private:
	GTKRam(void);
	GTKRam(GTKRam const &src);
	GTKRam &operator=(GTKRam const &rhs);
	GtkWidget *_infos;
};

#endif