#ifndef __CPU_MODULE_H_
#define __CPU_MODULE_H_

#include "AMonitorModule.hpp"

class CPUModule : public AMonitorModule {
public:
    CPUModule(void);
    ~CPUModule(void);

    VSTRING		getDatas(void);
    std::string GetStdoutFromCommand(std::string cmd);
private:
    CPUModule(CPUModule const &src);
    CPUModule &operator=(CPUModule const &rhs);
    double GetCPULoad();
    double CalculateCPULoad(unsigned long long idleTicks, unsigned long long totalTicks);
};

#endif
