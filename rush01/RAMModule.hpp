#ifndef __RAM_MODULE_H_
#define __RAM_MODULE_H_

#include "AMonitorModule.hpp"

class RAMModule : public AMonitorModule {
public:
	RAMModule(void);
	~RAMModule(void);

	VSTRING		getDatas(void);
private:
	RAMModule(RAMModule const &src);
	RAMModule &operator=(RAMModule const &rhs);
	double ParseMemValue(const char * b);
	float GetSystemMemoryUsagePercentage();
};

#endif
