#ifndef __NCURSES_OS_INFO_H_
#define __NCURSES_OS_INFO_H_

#include "ANcursesDisplayModule.hpp"

class NcursesOSInfo : public ANcursesDisplayModules {
private:
    NcursesOSInfo(NcursesOSInfo const &src);
    NcursesOSInfo &operator=(NcursesOSInfo const &rhs);

public:
    NcursesOSInfo(void);
    ~NcursesOSInfo(void);
    virtual void        display(VSTRING datas);
};


#endif
