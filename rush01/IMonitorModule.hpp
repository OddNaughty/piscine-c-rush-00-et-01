#ifndef __IMONITOR_MODULE_H_
#define __IMONITOR_MODULE_H_

#include "ft_gkrellm.hpp"
#include <string>
#include <vector>

typedef std::vector<std::string> VSTRING;

class IMonitorModule {
public:
	virtual VSTRING	getDatas(void) = 0;
	virtual std::string 	getType(void) = 0;
	~IMonitorModule(void) {};

};


#endif
