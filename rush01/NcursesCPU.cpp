#include <iostream>
#include <unistd.h>
#include "NcursesCPU.hpp"

NcursesCPU::NcursesCPU(void) : ANcursesDisplayModules(10, 50, 10, 50) {
}

NcursesCPU::~NcursesCPU() {}

void NcursesCPU::display(VSTRING datas) {
    wclear(window);
    const std::string name = "CPU";
    int     lines = 2;

    mvwprintw(window, 2, 0, "==================================================");
    mvwprintw(window, 1, (50 / 2) - (sizeof(name.c_str()) / 2),  name.c_str());
    datas[1] = "Core Nbr:\t" + datas[1];
    datas[0] = "Used %%:\t" + datas[0];
    datas[2] = "Model:\t" + datas[2];
    datas[3] = "Frequency:\t" + datas[3];
    for (int j = 0; j < (int) datas.size(); ++j) {
        mvwprintw(window, lines + 1, 1, datas[j].c_str());
        lines++;
    }
    box(window, 0, 0);
    wrefresh(window);
}