#include <sys/sysctl.h>
#include <iostream>
#include "AMonitorModule.hpp"
#include "OSInfoModule.hpp"


OSInfoModule::OSInfoModule(void) : AMonitorModule::AMonitorModule("OSInfoModule") {}

VSTRING			OSInfoModule::getDatas() {
	VSTRING infos;
	char buf[1024];
	size_t size = sizeof(buf);
	sysctlbyname("kern.ostype", &buf, &size, NULL, 0);
	infos.push_back(buf);
	sysctlbyname("kern.osrelease", &buf, &size, NULL, 0);
	infos.push_back(buf);
	return infos;
}