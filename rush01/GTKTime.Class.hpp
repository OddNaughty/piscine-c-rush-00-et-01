#ifndef GTKTIME_HPP
# define GTKTIME_HPP

#include <gtk/gtk.h>
#include "ANGTKDisplayModules.hpp"

class GTKTime : public ANGTKDisplayModules{
public:
	GTKTime(GtkWidget *ptr);
	~GTKTime(void);
	virtual void    display(VSTRING datas);
private:
	GTKTime(void);
	GTKTime(GTKTime const &src);
	GTKTime &operator=(GTKTime const &rhs);
	GtkWidget * _infos;
};

#endif