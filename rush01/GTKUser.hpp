#ifndef __GTK_USER_H_
#define __GTK_USER_H_

#include <gtk/gtk.h>


#include "ANGTKDisplayModules.hpp"

class GTKUser : public ANGTKDisplayModules {
public:
    GTKUser(GtkWidget *ptr);
    ~GTKUser(void);
    virtual void    display(VSTRING datas);
private:
    GTKUser(void);
    GTKUser(GTKUser const &src);
    GTKUser &operator=(GTKUser const &rhs);
    GtkWidget *_hostname;
    GtkWidget *_username;
};


#endif
