#ifndef __USER_MODULE_H_
#define __USER_MODULE_H_

#include "AMonitorModule.hpp"

class UserModule : public AMonitorModule {
public:
	UserModule(void);
	~UserModule(void);

	VSTRING		getDatas(void);
private:
	UserModule(UserModule const &src);
	UserModule &operator=(UserModule const &rhs);

};

#endif
